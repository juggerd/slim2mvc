<?php

namespace Controllers\Index;

class Logout extends \Raud\Controller {

    public function index() {
        $auth = \Zend_Auth::getInstance();
        $auth->clearIdentity();
        \Zend_Session::destroy();
        \Zend_Session::regenerateId();
        $this->app->redirect('/', 302);
    }

}
