<?php

namespace Controllers\Index;

class Index extends \Raud\Controller {

    public function init() {
        $auth = \Zend_Auth::getInstance();
        if (!$auth->getIdentity()) {
            $this->app->redirect('/login/', 302);
        }
    }

    public function index() {
        $this->view->title = "title";
        $this->view->keywords = "keywords";
        $this->view->description = "description";
        $this->view->user = \Zend_Auth::getInstance()->getIdentity();

        $model = new \Model_User();
        $this->view->users = $model->getUsers();
    }

}
