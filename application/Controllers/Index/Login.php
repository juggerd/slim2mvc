<?php

namespace Controllers\Index;

class Login extends \Raud\Controller {

    public function index() {
        if ($this->app->request()->isPost()) {
            $form = $this->app->request->params('form');
            //\Zend_Debug::dump($form);
            if (!$form['username'] || !$form['password']) {
                $this->view->message = 'Неправильные данные';
                return;
            }
            $authAdapter = new \Zend_Auth_Adapter_DbTable();
            $authAdapter
                    ->setTableName('user')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('MD5(?)');
            $authAdapter
                    ->setIdentity($form['username'])
                    ->setCredential($form['password']);

            $result = $authAdapter->authenticate();
            if ($result->isValid()) {
                $storage = new \Zend_Auth_Storage_Session();
                $storage->write($authAdapter->getResultRowObject());
                $this->app->redirect('/', 302);
            } else {
                $this->view->message = 'Неправильные данные';
            }
        }
    }

}
