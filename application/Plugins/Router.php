<?php

namespace Plugins;

class Router extends \Raud\Plugin {
    
    protected static $_instance;
    
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function register() {
        
        $this->app->notFound(function () {
            $this->app->render('views/404.phtml', [
                'layout' => 'layouts/404.phtml'
            ]);
        });
        
        $this->app->get('/', function() {
            $ctrl = new \Controllers\Index\Index();
            if (method_exists($ctrl, 'init')) $ctrl->init();
            if (method_exists($ctrl, 'index')) $ctrl->index();
            $this->app->render('views/index/index/index.phtml', []);
        });
        
        $this->app->get('/about/', function() {
            $ctrl = new \Controllers\Index\About();
            if (method_exists($ctrl, 'init')) $ctrl->init();
            if (method_exists($ctrl, 'index')) $ctrl->index();
            $this->app->render('views/index/about/index.phtml', []);
        });
        
        $this->app->map('/login/', function() {
            $ctrl = new \Controllers\Index\Login();
            if (method_exists($ctrl, 'init')) $ctrl->init();
            if (method_exists($ctrl, 'index')) $ctrl->index();
            $this->app->render('views/index/login/index.phtml', [
                'layout' => 'layouts/login.phtml'
            ]);            
        })->via('GET', 'POST');        
        
        $this->app->get('/logout/', function() {
            $ctrl = new \Controllers\Index\Logout();
            if (method_exists($ctrl, 'init')) $ctrl->init();
            if (method_exists($ctrl, 'index')) $ctrl->index();
        });          
    }
}
