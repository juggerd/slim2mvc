<?php

class Model_User extends \Zend_Db_Table_Abstract {

    protected $_name = "user";
    protected $_primary = 'id';

    public function getTableName() {
        return $this->_name;
    }

    public function getUsers() {
        return $this
                ->select()
                ->from($this->_name)
                ->query()
                ->fetchAll(Zend_Db::FETCH_OBJ);
    }

}
