CREATE DATABASE `slim2mvc`;

CREATE USER 'slim2mvc'@'localhost' IDENTIFIED BY  'slim2mvc';
GRANT USAGE ON *.* TO 'slim2mvc'@'localhost' IDENTIFIED BY  'slim2mvc';
GRANT ALL PRIVILEGES ON `slim2mvc`.* TO 'slim2mvc'@'localhost';
FLUSH PRIVILEGES;

USE `slim2mvc`;

CREATE TABLE `session` (
  `id` char(32),
  `modified` int(11),
  `lifetime` int(11),
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `group`, `username`, `password`, `realname`) VALUES (NULL, '0', 'admin', MD5('admin'), 'Администратор');