<?php

namespace Raud;

class Model {

    protected static $_instance;

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->app = \Slim\Slim::getInstance();
    }

}
