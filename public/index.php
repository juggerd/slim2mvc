<?php

define('VERSION', '0.4.1');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define('VENDOR_PATH', realpath(dirname(__FILE__) . '/../vendor'));

set_include_path(implode(PATH_SEPARATOR, [
    VENDOR_PATH,
    APPLICATION_PATH,
    get_include_path(),
]));

spl_autoload_register(function ($class) {
    if (preg_match('/Zend_/', $class)) {
        require_once VENDOR_PATH . '/' . str_replace('_', '/', $class) . '.php';
    } else if (preg_match('/Model_/', $class)) {
        require_once APPLICATION_PATH . '/' . str_replace('_', '/', str_replace('Model_', 'Models_', $class)) . '.php';
    } else {
        require_once str_replace('\\', '/', $class) . '.php';
    }
});

$app = new \Slim\Slim([
    'debug' => true,
    'mode' => 'development',
    'view' => '\Slim\LayoutView',
    'layout' => 'layouts/layout.phtml',
    'templates.path' => APPLICATION_PATH,
]);

\Plugins\Router::getInstance()->register();

$dbConfig = require_once(APPLICATION_PATH . '/configs/connect.db.php');
$dbAdapter = \Zend_Db::factory('Pdo_Mysql', $dbConfig);
\Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);

\Zend_Session::setOptions([
    'gc_probability' => 5,
    'gc_divisor' => 100,
    'gc_maxlifetime' => 86400
]);

\Zend_Session::setSaveHandler(new \Zend_Session_SaveHandler_DbTable([
    'name' => 'session',
    'primary' => 'id',
    'modifiedColumn' => 'modified',
    'dataColumn' => 'data',
    'lifetimeColumn' => 'lifetime'
]));

\Zend_Session::start();
$app->run();
